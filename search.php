<?php

/**

 * The template for displaying search results page.

 */

get_header(); ?>

<section class="page_banner" style="background-image:url('<?php echo esc_url(get_template_directory_uri()); ?>/images/influencer-arketing-banner.jpg');"></section>




<section class="search_page py-5 mb-4 mt-md-5 mt-4">

  <div class="container">

    <div class="row">


      <div class="col-md-12">

        <div class="search_result">

          <?php if (have_posts()) : ?>

            <div class="result pb-4">
              <h3><?php _e('Search Results for: ', 'your-theme'); ?><b><?php the_search_query(); ?></b></h3>
            </div>

            <?php global $wp_query;
            $total_pages = $wp_query->max_num_pages;
            if ($total_pages > 1) { ?>

              <div id="nav-above" class="navigation">

                <div class="nav-previous"><?php next_posts_link(__('<span class="meta-nav">&laquo;</span> Older posts', 'your-theme')) ?></div>

                <div class="nav-next"><?php previous_posts_link(__('Newer posts <span class="meta-nav">&raquo;</span>', 'your-theme')) ?></div>

              </div><!-- #nav-above -->

            <?php } ?>

            <?php while (have_posts()) : the_post() ?>

              <div class="pb-3" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <h4 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf(__('Permalink to %s', 'your-theme'), the_title_attribute('echo=0')); ?>" rel="bookmark"><?php the_title(); ?></a></h4>

                <?php if ($post->post_type == 'post') { ?>

                  <div class="entry-meta">

                    <span class="meta-prep meta-prep-author"><?php _e('By ', 'your-theme'); ?></span>

                    <span class="author vcard"><a class="url fn n" href="<?php echo get_author_link(false, $authordata->ID, $authordata->user_nicename); ?>" title="<?php printf(__('View all posts by %s', 'your-theme'), $authordata->display_name); ?>"><?php the_author(); ?></a></span>

                    <span class="meta-sep"> | </span>

                    <span class="meta-prep meta-prep-entry-date"><?php _e('Published ', 'your-theme'); ?></span>

                    <span class="entry-date"><abbr class="published" title="<?php the_time('Y-m-d\TH:i:sO') ?>"><?php the_time(get_option('date_format')); ?></abbr></span>

                    <?php edit_post_link(__('Edit', 'your-theme'), "<span class=\"meta-sep\">|</span>\n\t\t\t\t\t\t<span class=\"edit-link\">", "</span>\n\t\t\t\t\t") ?>

                  </div><!-- .entry-meta -->

                <?php } ?>

                <div class="entry-summary">

                  <?php the_excerpt(__('Continue reading <span class="meta-nav">&raquo;</span>', 'your-theme')); ?>

                  <?php wp_link_pages('before=<div class="page-link">' . __('Pages:', 'your-theme') . '&after=</div>') ?>

                </div><!-- .entry-summary -->

                <?php if ($post->post_type == 'post') { ?>

                  <div class="entry-utility">

                    <span class="cat-links"><span class="entry-utility-prep entry-utility-prep-cat-links"><?php _e('Posted in ', 'your-theme'); ?></span><?php echo get_the_category_list(', '); ?></span>

                    <span class="meta-sep"> | </span>

                    <?php the_tags('<span class="tag-links"><span class="entry-utility-prep entry-utility-prep-tag-links">' . __('Tagged ', 'your-theme') . '</span>', ", ", "</span>\n\t\t\t\t\t\t<span class=\"meta-sep\">|</span>\n") ?>

                    <span class="comments-link"><?php comments_popup_link(__('Leave a comment', 'your-theme'), __('1 Comment', 'your-theme'), __('% Comments', 'your-theme')) ?></span>

                    <?php edit_post_link(__('Edit', 'your-theme'), "<span class=\"meta-sep\">|</span>\n\t\t\t\t\t\t<span class=\"edit-link\">", "</span>\n\t\t\t\t\t\n") ?>

                  </div><!-- #entry-utility -->

                <?php } ?>

              </div><!-- #post-<?php the_ID(); ?> -->

            <?php endwhile; ?>

            <?php global $wp_query;
            $total_pages = $wp_query->max_num_pages;
            if ($total_pages > 1) { ?>

              <div id="nav-below" class="navigation">

                <div class="nav-previous"><?php next_posts_link(__('<span class="meta-nav">&laquo;</span> Older posts', 'your-theme')) ?></div>

                <div class="nav-next"><?php previous_posts_link(__('Newer posts <span class="meta-nav">&raquo;</span>', 'your-theme')) ?></div>

              </div><!-- #nav-below -->

            <?php } ?>

          <?php else : ?>

            <div id="post-0" class="post no-results not-found">

              <h3 class="entry-title"><?php _e('Nothing Found', 'your-theme') ?></h3>

              <div class="entry-content">

                <p><?php _e('Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'your-theme'); ?></p>

                <!-- <?php //get_search_form(); 
                      ?> -->
                <div class="search_form_page">
                  <form id="labnol" class="" method="get" action="<?php echo esc_url(home_url('/')); ?>">
                    <div class="speech">
                      <img class="hold-me" onclick="startDictation()" src="https://image.flaticon.com/icons/svg/26/26312.svg" />
                      <input type="text" class="search-query form-control" value="<?php echo get_search_query(); ?>" name="s" id="transcript" />
                    </div>
                  </form>
                </div>
              </div><!-- .entry-content -->

            </div>

          <?php endif; ?>

        </div>


      </div>

      <!--<div class="col-md-4 sidebar">

<?php //get_sidebar(); 
?>

</div>-->

    </div>

  </div>

</section>

<?php get_footer(); ?>