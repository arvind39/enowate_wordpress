<?php
/**
/* Template Name: About Us
 *
 * Displays Only about template
 
 * @package WordPress
 * @subpackage enowate
 * @since enowate 1.0
 */
 get_header(); ?>

<?php

$post1 = get_post(280);
$post1_title = $post1->post_title;
$post1_text = $post1->post_content;
$post1_img = get_the_post_thumbnail_url($post1, 'thumbnail');


$post2 = get_post(284);
$post2_title = $post2->post_title;
$post2_text = $post2->post_content;
$post2_img = get_the_post_thumbnail_url($post2, 'thumbnail');


$post3 = get_post(285);
$post3_title = $post3->post_title;
$post3_text = $post3->post_content;
$post3_img = get_the_post_thumbnail_url($post3, 'thumbnail');

$post4 = get_post(286);
$post4_title = $post4->post_title;
$post4_text = $post4->post_content;
$post4_img = get_the_post_thumbnail_url($post4, 'thumbnail');

$post5 = get_post(287);
$post5_title = $post5->post_title;
$post5_text = $post5->post_content;
$post5_img = get_the_post_thumbnail_url($post5, 'thumbnail');

?>


<?php
$image_url = wp_get_attachment_url(get_post_thumbnail_id());
if (!empty(get_the_post_thumbnail())) {
?>
  <section class="page_banner" style="background-image:url('<?php echo $image_url; ?>"></section>
<?php } else { ?>
  <section class="page_banner" style="background-image:url('<?php echo esc_url( get_template_directory_uri() ); ?>/images/about-us-banner.jpg');">

  </section>
<?php } ?>

<!--********** -->

  
  <section class="about-us-sec1 pt-md-5 mb-xl-4 mb-lg-5 mb-4 mt-md-4 mt-5">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-md-12">
          <div class="default_title pb-xxl-4">
            <h2><?php echo $post1_title ?></h2>
            <p><?php echo $post1_text ?></p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="about-us-sec2 py-md-4">
    <div id="about-slider" class="owl-carousel">
    <?php $posts = new WP_Query(array('post_type' => 'about_us', 'category_name' => 'gallery',  'order' => 'ASC', 'posts_per_page' => '-1')); ?>
      <?php while ($posts->have_posts()) : $posts->the_post(); ?>
      <div class="brand_icon">
        <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="img-fluid">
      </div>

      <?php endwhile; ?>

      <!-- <div class="brand_icon">
        <img src="<?php //echo esc_url( get_template_directory_uri() ); ?>/images/about-img2.jpg" class="img-fluid">
      </div>
     -->

    </div>
  </section>
  <section class="about-us-sec3">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="box_grid bg_yellow">

            <div class="row align-items-center justify-content-center">
              <div class="col-lg-4 col-md-12">
                <div class="content_box wow fadeInUp">
                  <h2 class="pb-2"><?php echo $post2_title ?> </h2>
                  <p><?php echo $post2_text ?> </p>

                </div>
              </div>
              <div class="col-lg-8 col-md-12">
                <div class="row align-items-center">
                  <div class="col-lg-6 col-md-12">
                    <div class="icon_box box_style3 px-4 px-xxl-5 pt-xxl-5 pt-4 pb-xxl-4 pb-2">
                      <div class="icon mb-md-4 mb-3">
                        <img src="<?php echo $post3_img ?>" class="img-fluid">
                      </div>
                      <h4><?php echo $post3_title ?> </h4>
                      <small><?php echo $post3_text ?></small> </p>
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-12">
                    <div class="box_up_down">
                      <div class="icon_box box_style3 up_box px-4 px-xxl-5 pt-xxl-5 pt-4 pb-xxl-4 pb-2">
                        <div class="icon mb-md-4 mb-3">
                          <img src="<?php echo $post4_img ?>" class="img-fluid">
                        </div>
                        <h4><?php echo $post4_title ?></h4>
                        <small><?php echo $post4_text ?>  </small> </p>
                      </div>
                      <div class="icon_box box_style3 down_box  px-4 px-xxl-5 pt-xxl-5 pt-4 pb-xxl-4 pb-2">
                        <div class="icon mb-md-4 mb-3">
                          <img src="<?php echo $post5_img ?>" class="img-fluid">
                        </div>
                        <h4><?php echo $post5_title ?></h4>
                        <small><?php echo $post5_text ?></small> </p>
                      </div>
                    </div>

                  </div>
                </div>

              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="about-us-sec4 py-5">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-xxl-11 col-md-12">
          <div class="default_title text-center pb-5">
            <h2>Our experts</h2>
          </div>
          <div class="profile_grid">
            <div class="row">
            <?php $posts = new WP_Query(array('post_type' => 'about_us', 'category_name' => '	Our Team',  'order' => 'ASC', 'posts_per_page' => '6')); ?>
              <?php while ($posts->have_posts()) : $posts->the_post(); ?>
              <div class="col-md-4">
                <div class="profile_box mb-lg-5 mb-4">
                  <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="img-fluid">
                  <div class="pt-4">
                    <h4><?php echo get_the_title(); ?></h4>
                    <p><?php echo get_the_content(); ?></p>
                  </div>
                </div>
              </div>
              <?php endwhile; ?>
              <!-- <div class="col-md-4">
                <div class="profile_box mb-lg-5 mb-4">
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/profile-2.jpg" class="img-fluid">
                  <div class="pt-4">
                    <h4>Ash Gaylard</h4>
                    <p>Digital strategy lead</p>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="profile_box mb-lg-5 mb-4">
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/profile-3.jpg" class="img-fluid">
                  <div class="pt-4">
                    <h4>Glenn Lockwood</h4>
                    <p>Founder & director</p>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="profile_box mb-lg-5 mb-4">
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/profile-4.jpg" class="img-fluid">
                  <div class="pt-4">
                    <h4>Kelley Franklin</h4>
                    <p>Digital strategy lead</p>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="profile_box mb-lg-5 mb-4">
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/profile-5.jpg" class="img-fluid">
                  <div class="pt-4">
                    <h4>Alex Pape</h4>
                    <p>Lead content director</p>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="profile_box mb-lg-5 mb-4">
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/profile-6.jpg" class="img-fluid">
                  <div class="pt-4">
                    <h4>Lindsey Haag</h4>
                    <p>Social media marketer</p>
                  </div>
                </div>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="about-us-sec5 bg_yellow">
    <div class="container">
      <div class="col-md-12">
        <div class="default_title pb-md-3">
          <h2>Why choose us?</h2>
          <p>Not one, not two, we give you plenty of reasons to choose us.</p>
        </div>
      </div>
      <div class="row align-items-center">
      <?php $posts = new WP_Query(array('post_type' => 'about_us', 'category_name' => 'why choose us',  'order' => 'ASC', 'posts_per_page' => '6')); ?>
        <?php while ($posts->have_posts()) : $posts->the_post(); ?>
        <div class="col-md-4">
          <div class="icon_box mb-md-4 mb-2">
            <div class="icon mb-md-4 mb-md-3">
              <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="img-fluid">
            </div>
            <h4><?php echo get_the_title(); ?></h4>
          </div>
        </div>
        <?php endwhile; ?>
        <!-- <div class="col-md-4">
          <div class="icon_box mb-md-4 mb-2">
            <div class="icon mb-md-4 mb-md-3">
              <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icons/c2.svg" class="img-fluid">
            </div>
            <h4>Programmatic Marketing</h4>
          </div>
        </div>
       -->
      </div>
    </div>
  </section>

  <?php get_footer(); ?>