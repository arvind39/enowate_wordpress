<?php
/**
/* Template Name: Product Placement
 *
 *
 *
 * @package WordPress
 * @subpackage enowate
 * @since enowate 1.0
 */
 get_header(); ?>


<?php

//Section 1
$post1 = get_post(122);
$post1_title = $post1->post_title;
$post1_text = $post1->post_content;
$post1_img = get_the_post_thumbnail_url($post1, 'thumbnail');

// Section 2
$post2 = get_post(123);
$post2_title = $post2->post_title;
$post2_text = $post2->post_content;
$post2_img = get_the_post_thumbnail_url($post2, 'thumbnail');

// Section 3
$post3 = get_post(124);
$post3_title = $post3->post_title;
$post3_text = $post3->post_content;
$post3_img = get_the_post_thumbnail_url($post3, 'thumbnail');

// Section 4
$post4 = get_post(203);
$post4_title = $post4->post_title;
$post4_text = $post4->post_content;
$post4_img = get_the_post_thumbnail_url($post4, 'thumbnail');

?>




<!--Banner Part-->

<?php
  $image_url= wp_get_attachment_url( get_post_thumbnail_id() );
  if( !empty(get_the_post_thumbnail()) ) {
?>
<section class="page_banner" style="background-image:url('<?php echo $image_url;?>"></section>
<?php } else { ?>
  <section class="page_banner" style="background-image:url('<?php echo esc_url( get_template_directory_uri() ); ?>/images/product-placement-banner.jpg');"></section>
<?php } ?>

<!--********** -->
 
  <section class="product-placement-sec1 py-5 mb-md-4 mt-md-0 mt-4">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-6">
          <div class="content_box pe-xxl-5 wow fadeIn" >
            <h2 class="pb-2"><?php echo $post1_title?></h2>            
            <p><?php echo $post1_text ?></p>

          </div>
        </div>
        <div class="col-md-6">
          <div class="img_box p-4 p-lg-0 pt-5 pt-md-0 wow zoomIn text-end">
            <img src="<?php echo $post1_img ?>" class="img-fluid">
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="product-placement-sec2 bg_yellow py-5 ">
    <div class="container">
      <div class="row align-items-center flex-column-reverse flex-md-row mt-md-0 mt-4">

        <div class="col-md-6">
          <div class="img_box p-4 p-lg-0 pt-5 pt-md-0 wow zoomIn">
            <img src="<?php echo $post2_img ?>" class="img-fluid">
          </div>
        </div>
        <div class="col-md-6">
          <div class="content_box  wow fadeIn ps-xxl-5">
            <h2 class="pb-2"><?php echo $post2_title?></h2>
            <p><?php echo $post2_text ?> </p>

          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="product-placement-sec3 py-5 mt-md-5 mt-4 mb-md-4">
    <div class="container">
      <div class="col-md-12">
        <div class="default_title text-center mb-5">
          <h2>How we create difference</h2>         
        </div>
      </div>
      <div class="row align-items-center">
        <div class="col-md-6">
          <div class="icon_box">

            <div class="icon mb-md-4 mb-3">
              <img src="<?php echo $post3_img ?>" class="img-fluid">
            </div>
            <h4><?php echo $post3_title?></h4>
            <small><?php echo $post3_text ?></small> </p>

          </div>
        </div>  
        <div class="col-md-6">
          <div class="icon_box">

            <div class="icon mb-md-4 mb-3">
              <img src="<?php echo $post4_img ?>" class="img-fluid">
            </div>
            <h4><?php echo $post4_title?></h4>
            <small><?php echo $post4_text ?></small>

          </div>
        </div>     
      </div>
    </div>
  </section>
  <?php get_footer(); ?>