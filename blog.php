<?php
/**
/* Template Name: Blogs Page
 *
 *
 *
 * @package WordPress
 * @subpackage enowate
 * @since enowate 1.0
 */
 get_header(); ?>


<!--Banner Part-->

<?php
  $image_url= wp_get_attachment_url( get_post_thumbnail_id() );
  if( !empty(get_the_post_thumbnail()) ) {
?>
<section class="page_banner" style="background-image:url('<?php echo $image_url;?>"></section>
<?php } else { ?>
  <section class="page_banner" style="background-image:url('<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog-banner.jpg');">

</section>
<?php } ?>

<!--********** -->


  
  <section class="blog-sec1 py-5 my-4">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-xxl-9 col-lg-11 col-md-12">
          <div class="content_box text-center wow fadeIn">
            <h2 class="pb-2 px-lg-4">Blogs</h2>
            <p>Suspendisse pulvinar fermentum rutrum. In vel risus egestas, aliquam purus nec, maximus tortor. </p>
          </div>
        </div>
        <div class="col-md-12 col-xxl-11">
          <div class="blog_list pt-5">
            <div class="row">


            <?php $posts = new WP_Query(array('post_type' => 'blog', 'order' => 'ASC', 'posts_per_page' => '-1')); ?>

              <?php while ($posts->have_posts()) : $posts->the_post(); ?>
              <div class="col-md-6 col-lg-4">
                <div class="blog_box blog_style2 mb-5">
                  <a href="<?php the_permalink(); ?>">
                    <div class="img_box">
                      <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" class="img-fluid">
                    </div>
                    
                    <div class="blog_title">
                      <div class="d-flex align-items-center mb-2">
                      <?php $categories = get_the_category();
                      if ( ! empty( $categories ) ) {
                          echo '<span>' . esc_html( $categories[0]->name ) . '</span>';
                      }?>
                        <p><?php echo get_the_date(); ?></p>
                      </div>
                      <h3><?php echo get_the_title(); ?></h3>

                    </div>
                  </a>
                </div>
              </div>

              <?php endwhile; ?>

              <!-- <div class="col-md-6 col-lg-4">
                <div class="blog_box blog_style2 mb-5">
                  <a href="single-blog.html">
                    <div class="img_box">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog2.jpg" class="img-fluid">
                    </div>
                    
                    <div class="blog_title">
                      <div class="d-flex align-items-center mb-2">
                        <span>PPC</span>
                        <p>July 28, 2021</p>
                      </div>
                      <h3>Doloremque laudantium, totam rem aperiam, eaque ipsa quae.</h3>

                    </div>
                  </a>
                </div>

              </div> -->
              <!-- <div class="col-md-6 col-lg-4">
                <div class="blog_box blog_style2 mb-5">
                  <a href="single-blog.html">
                    <div class="img_box">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog3.jpg" class="img-fluid">
                    </div>
                    
                    <div class="blog_title">
                      <div class="d-flex align-items-center mb-2">
                        <span>SEM</span>
                        <p>July 27, 2021</p>
                      </div>
                      <h3>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut.</h3>
                    </div>
                  </a>
                </div>
              </div>
              <div class="col-md-6 col-lg-4">
                <div class="blog_box blog_style2 mb-5">                  
                  <a href="single-blog.html">
                    <div class="img_box">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog4.jpg" class="img-fluid">
                    </div>                    
                    <div class="blog_title">
                      <div class="d-flex align-items-center mb-2">
                        <span>SEM</span>
                        <p>July 27, 2021</p>
                      </div>
                      <h3>Doloremque laudantium, totam rem aperiam, eaque ipsa quae.</h3>
                    </div>
                  </a>
                </div>
              </div>
              <div class="col-md-6 col-lg-4">
                <div class="blog_box blog_style2 mb-5">
                  <a href="single-blog.html">
                    <div class="img_box">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog5.jpg" class="img-fluid">
                    </div>
                    
                    <div class="blog_title">
                      <div class="d-flex align-items-center mb-2">
                        <span>SEM</span>
                        <p>July 27, 2021</p>
                      </div>
                      <h3>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut.</h3>
                    </div>
                  </a>
                </div>
              </div>
              <div class="col-md-6 col-lg-4">
                <div class="blog_box blog_style2 mb-5">
                  <a href="single-blog.html">
                    <div class="img_box">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog6.jpg" class="img-fluid">
                    </div>
                    
                    <div class="blog_title">
                      <div class="d-flex align-items-center mb-2">
                        <span>SEM</span>
                        <p>July 27, 2021</p>
                      </div>
                      <h3>Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</h3>
                    </div>
                  </a>
                </div>
              </div>
              <div class="col-md-6 col-lg-4">
                <div class="blog_box blog_style2 mb-5">
                  <a href="single-blog.html">
                    <div class="img_box">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog7.jpg" class="img-fluid">
                    </div>
                    
                    <div class="blog_title">
                      <div class="d-flex align-items-center mb-2">
                        <span>SEM</span>
                        <p>July 27, 2021</p>
                      </div>
                      <h3>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut.</h3>
                    </div>
                  </a>
                </div>
              </div>
              <div class="col-md-6 col-lg-4">
                <div class="blog_box blog_style2 mb-5">
                  <a href="single-blog.html">
                    <div class="img_box">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog8.jpg" class="img-fluid">
                    </div>
                    
                    <div class="blog_title">
                      <div class="d-flex align-items-center mb-2">
                        <span>SEM</span>
                        <p>July 27, 2021</p>
                      </div>
                      <h3>Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</h3>
                    </div>
                  </a>
                </div>
              </div>
              <div class="col-md-6 col-lg-4">
                <div class="blog_box blog_style2 mb-5">
                  <a href="single-blog.html">
                    <div class="img_box">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog9.jpg" class="img-fluid">
                    </div>
                    
                    <div class="blog_title">
                      <div class="d-flex align-items-center mb-2">
                        <span>SEM</span>
                        <p>July 27, 2021</p>
                      </div>
                      <h3>Doloremque laudantium, totam rem aperiam, eaque ipsa quae.</h3>
                    </div>
                  </a>
                </div>
              </div>              
              <div class="col-md-6 col-lg-4">
                <div class="blog_box blog_style2 mb-5">
                  <a href="single-blog.html">
                    <div class="img_box">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog5.jpg" class="img-fluid">
                    </div>
                    
                    <div class="blog_title">
                      <div class="d-flex align-items-center mb-2">
                        <span>SEM</span>
                        <p>July 27, 2021</p>
                      </div>
                      <h3>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut.</h3>
                    </div>
                  </a>
                </div>
              </div>
              <div class="col-md-6 col-lg-4">
                <div class="blog_box blog_style2 mb-5">
                  <a href="single-blog.html">
                    <div class="img_box">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog6.jpg" class="img-fluid">
                    </div>
                    
                    <div class="blog_title">
                      <div class="d-flex align-items-center mb-2">
                        <span>SEM</span>
                        <p>July 27, 2021</p>
                      </div>
                      <h3>Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</h3>
                    </div>
                  </a>
                </div>
              </div>
              <div class="col-md-6 col-lg-4">
                <div class="blog_box blog_style2 mb-5">
                  <a href="single-blog.html">
                    <div class="img_box">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog7.jpg" class="img-fluid">
                    </div>
                    
                    <div class="blog_title">
                      <div class="d-flex align-items-center mb-2">
                        <span>SEM</span>
                        <p>July 27, 2021</p>
                      </div>
                      <h3>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut.</h3>
                    </div>
                  </a>
                </div>
              </div>
              <div class="col-md-6 col-lg-4">
                <div class="blog_box blog_style2 mb-5">
                  <a href="single-blog.html">
                    <div class="img_box">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog8.jpg" class="img-fluid">
                    </div>
                    
                    <div class="blog_title">
                      <div class="d-flex align-items-center mb-2">
                        <span>SEM</span>
                        <p>July 27, 2021</p>
                      </div>
                      <h3>Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</h3>
                    </div>
                  </a>
                </div>
              </div>
              <div class="col-md-6 col-lg-4">
                <div class="blog_box blog_style2 mb-5">
                  <a href="single-blog.html">
                    <div class="img_box">
                      <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog9.jpg" class="img-fluid">
                    </div>
                    
                    <div class="blog_title">
                      <div class="d-flex align-items-center mb-2">
                        <span>SEM</span>
                        <p>July 27, 2021</p>
                      </div>
                      <h3>Doloremque laudantium, totam rem aperiam, eaque ipsa quae.</h3>
                    </div>
                  </a>
                </div>
              </div> -->
              
            </div>           
            
          </div>
        </div>
        <div class="col-md-12">
          <div class="pagination text-center ">
            <div class="w-100" id="custom_pagination"></div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php get_footer(); ?>
  
<script>
  var items = $(".blog_list .col-md-6");
  var numItems = items.length;
  var perPage =9;
  items.slice(perPage).hide();
  $('#custom_pagination').pagination({
    items: numItems,
    itemsOnPage: perPage,
    prevText: "&laquo;",
    nextText: "&raquo;",
    onPageClick: function (pageNumber) {
      var showFrom = perPage * (pageNumber - 1);
      var showTo = showFrom + perPage;
      items.hide().slice(showFrom, showTo).show();
    }
  });
</script>
