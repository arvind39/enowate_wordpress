<?php
/**
/* Template Name: Contact Page
 *
 *
 *
 * @package WordPress
 * @subpackage enowate
 * @since enowate 1.0
 */
 get_header(); ?>
  <section class="page_banner" style="background-image:url('<?php echo esc_url( get_template_directory_uri() ); ?>/images/contact-banner.jpg');">

  </section>
  <section id="contactform" class="cobntact-sec1 py-5 my-lg-4 mt-3 mb-2">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-xxl-9 col-lg-11 col-md-12">
          <div class="content_box text-center wow fadeIn">
            <h2 class="pb-2 px-lg-4">Excited about the project?</h2>
            <p>Discuss your business, audience, goal, and budget to get the best marketing strategies. Fill the form,
              send the queries, and we’ll contact you super soon.</p>
          </div>
        </div>
        <div class="col-md-12 col-xxl-11">
          <div class="contact_box pt-md-5 pt-4">
            <div class="row">
              <div class="col-md-5">
                <div class="address_box">
                  <div class="default_title wow fadeInUp">
                    <span>Connect</span>
                    <h2 class="my-4">Let's Talk</h2>                   
                  </div>

                  <ul>
                    <li>
                      <p>Phone no</p>
                      <a href="tel:5127754199">(512) 775-4199</a>
                    </li>
                    <li>
                      <p>Email</p>
                      <a href="mailto:contact@enowate.com">contact@enowate.com</a>
                    </li>
                    <li>
                      <p>Mailing Address</p>
                      <h5>3783 Maloy Court, Beverly,<br>
                        KS, USA</h5>
                    </li>
                  </ul>

                </div>
              </div>
              <div class="col-md-7">
                <div class="contact_form form_box pt-md-5 mt-4">
                  <!-- <form action="javascript:void(0)">

                    <div class="row">
                      <div class="col-md-6 col-12">
                        <fieldset class="mb-5">

                          <input type="text" name="name" class="form-control" placeholder="Full Name*" required>
                        </fieldset>
                      </div>

                      <div class="col-md-6 col-12">
                        <fieldset class="mb-5">

                          <input type="text" name="phone-number" pattern="\d{10}" title="Only 10 digits mobile number"
                            class="form-control" placeholder="Phone No*" required>
                        </fieldset>
                      </div>
                      <div class="col-md-12 col-12">
                        <fieldset class="mb-5">
                          <input type="email" class="form-control" placeholder="Email Address*" required>
                        </fieldset>
                      </div>

                      <div class="col-md-8 col-xl-9 col-12">
                        <fieldset class="mb-4">

                          <textarea cols="10" rows="3" class="form-control shadow-none" minlength="25"
                            placeholder="Your Message*" required></textarea>
                        </fieldset>
                      </div>

                      <div class="col-md-3 col-xl-3 col-12 d-flex align-items-lg-center align-items-end">
                        <div class="send_btn">
                          <input type="submit" class="input_field_send" value="Send">
                          <span>SEND</span>
                          <svg class="ms-3" width="10" height="10" viewBox="0 0 10 10" fill="#000"
                            xmlns="http://www.w3.org/2000/svg">
                            <path d="M3.45999 10V6.3H0V3.68H3.45999V0H6.25999V3.68H9.72V6.3H6.25999V10H3.45999Z"></path>
                          </svg>
                        </div>
                      </div>
                    </div>
                  </form> -->
                  <?php echo do_shortcode('[contact-form-7 id="304" title="Contact form"]');?>
                </div>
              </div>

            </div>


          </div>
        </div>

      </div>
    </div>
  </section>

  <?php get_footer(); ?>