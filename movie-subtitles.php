<?php
/**
/* Template Name: Movie Subtitles
 *
 *
 *
 * @package WordPress
 * @subpackage enowate
 * @since enowate 1.0
 */
 get_header(); ?>

<?php

//Section 1
$post1 = get_post(216);
$post1_title = $post1->post_title;
$post1_text = $post1->post_content;
$post1_img = get_the_post_thumbnail_url($post1, 'thumbnail');

// Section 2
$post2 = get_post(217);
$post2_title = $post2->post_title;
$post2_text = $post2->post_content;
$post2_img = get_the_post_thumbnail_url($post2, 'thumbnail');

// Section 5
$post3 = get_post(221);
$post3_title = $post3->post_title;
$post3_text = $post3->post_content;
$post3_img = get_the_post_thumbnail_url($post3, 'thumbnail');

// Section 7
$post4 = get_post(222);
$post4_title = $post4->post_title;
$post4_text = $post4->post_content;
$post4_img = get_the_post_thumbnail_url($post4, 'thumbnail');

// Section 7
$post5 = get_post(224);
$post5_title = $post5->post_title;
$post5_text = $post5->post_content;
$post5_img = get_the_post_thumbnail_url($post5, 'thumbnail');

?>

<!--Banner Part-->

<?php
  $image_url= wp_get_attachment_url( get_post_thumbnail_id() );
  if( !empty(get_the_post_thumbnail()) ) {
?>
<section class="page_banner" style="background-image:url('<?php echo $image_url;?>"></section>
<?php } else { ?>
  <section class="page_banner" style="background-image:url('<?php echo esc_url( get_template_directory_uri() ); ?>/images/movie-subtitles-banner.jpg');">

  </section>
<?php } ?>

<!--********** -->


  
  <section class="movie-subtitle-sec1 pt-md-5  mb-xxl-4 mb-lg-5 mt-md-4 mt-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6 col-md-12 col-xxl-5">
          <div class="content_box wow fadeIn">
            <h2 class="pb-2"><?php echo $post1_title ?></h2>
            <p><?php echo $post1_text ?></p>

          </div>
        </div>
        <div class="col-lg-6 col-md-12 col-xxl-7">
          <div class="img_box  pt-4 pt-md-0 wow zoomIn text-end">
            <img src="<?php echo $post1_img ?>" class="img-fluid">
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="movie-subtitle-sec2 bg_yellow pt-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="default_title text-center py-4">
            <h2><?php echo $post2_title ?></h2>
          </div>
          <div class="content_box text-center wow fadeIn px-lg-5">
            <p><?php echo $post2_text ?> </p>
          </div>
          <div class="img_box p-4 p-lg-0 pt-5 pt-md-0 text-center">
            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/img4.png" class="img-fluid img_move1">
            <img src="<?php echo $post2_img ?>" class="img-fluid img_move2">
          </div>

        </div>
      </div>

    </div>
  </section>

  <section class="movie-subtitle-sec3">
    <div class="container">
      <div class="row">
      <?php $posts = new WP_Query(array('post_type' => 'movie_subtitles', 'category_name' => 'Movie Subtitles',  'order' => 'ASC', 'posts_per_page' => '3' )); ?>

<?php while ($posts->have_posts()) : $posts->the_post(); ?>
        <div class="col-md-4">
          <div class="icon_box_2 text-center mb-5 mb-md-0">
            <div class="icon mb-md-4 mb-3">
              <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" class="img-fluid">
            </div>
            <h4><?php echo get_the_title(); ?></h4>
            <div class="px-xxl-5 px-xl-4"><small><?php echo get_the_content(); ?></small> </div>
          </div>
        </div>
        <?php endwhile; ?>
        <!-- <div class="col-md-4">
          <div class="icon_box_2 text-center mb-5 mb-md-0">
            <div class="icon mb-md-4 mb-3">
              <img src="<?php //echo esc_url( get_template_directory_uri() ); ?>/images/img7.jpg" class="img-fluid">
            </div>
            <h4>Subtitles Format</h4>
            <p class="px-xxl-5 px-xl-4"><small>We create subtitles in different formats, and these formats get further tailored for
                broadcast, video on demand, film, social media, and e-learning content. </small> </p>
          </div>
        </div>
        <div class="col-md-4">
          <div class="icon_box_2 text-center mb-5 mb-md-0">
            <div class="icon mb-md-4 mb-3">
              <img src="<?php //echo esc_url( get_template_directory_uri() ); ?>/images/img8.jpg" class="img-fluid">
            </div>
            <h4>Subtitling Process</h4>
            <p class="px-xxl-5 px-xl-4"><small>Our subtitling process includes the addition of text to any audio-visual media to
                represent the spoken messages. In addition, our written subtitles help people to read and
                understand.</small> </p>
          </div>
        </div> -->
      </div>

    </div>
  </section>
  <section class="movie-subtitle-sec4 pb-4 pb-md-0 mb-md-4 ">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-6">
          <div class="content_box pe-xxl-5 wow fadeIn">
            <h2 class="pb-2"><?php echo $post3_title ?></h2>
            <p><?php echo $post3_text ?></p>

          </div>
        </div>
        <div class="col-md-6 mt-4 mt-md-0">
          <div class="button_box ps-xxl-5 d-flex flex-column align-items-center justify-content-end wow zoomIn ">
            <a href="javascript:void(0)" class="cta_2 mb-4">
              Broadcast Subtitle Formats
            </a>
            <a href="javascript:void(0)" class="cta_2">
              Online Media Subtitle Formats
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="movie-subtitle-sec5 bg_dark text-center mt-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="default_title text_white mb-lg-5 mb-4">
            <h2>Start your subtitling project today!</h2>
          </div>
          <a href="<?php echo get_site_url(); ?>/contact#contactform" class="cta_btn cta_big">
            HIRE US TODAY <svg class="
            ms-3" width="10" height="10" viewBox="0 0 10 10" fill="#000" xmlns="http://www.w3.org/2000/svg">
              <path d="M3.45999 10V6.3H0V3.68H3.45999V0H6.25999V3.68H9.72V6.3H6.25999V10H3.45999Z"></path>
            </svg>
          </a>

        </div>
      </div>

    </div>
  </section>
  <section class="movie-subtitle-sec6 py-xl-5 mt-5 pt-md-4 mb-5">
    <div class="container">
      <div class="col-md-12">
        <div class="default_title text-center mb-lg-5 mb-4">
          <h2>How we create difference</h2>
        </div>
      </div>
      <div class="row align-items-center">
        <div class="col-md-6">
          <div class="icon_box px-xxl-4">

            <div class="icon mb-md-4 mb-3">
              <img src="<?php echo $post4_img ?>" class="img-fluid">
            </div>
            <h4><?php echo $post4_title ?></h4>
            <small><?php echo $post4_text ?></small> </p>

          </div>
        </div>
        <div class="col-md-6">
          <div class="icon_box px-xxl-4">

            <div class="icon mb-md-4 mb-3">
              <img src="<?php echo $post5_img ?>" class="img-fluid">
            </div>
            <h4><?php echo $post5_title ?></h4>
            <small><?php echo $post5_text ?></small>

          </div>
        </div>
      </div>
    </div>
  </section>
  <?php get_footer(); ?>