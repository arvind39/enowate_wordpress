
//wow animation

new WOW().init();

/***mobile menu icon****/

$(document).mouseup(function (e) {
  var container = $(".navbar-toggler");
  $('.navbar-toggler').toggleClass('change');
  // If the target of the click isn't the container
  if (!container.is(e.target) && container.has(e.target).length === 0) {
    $('.navbar-toggler').removeClass('change');
    $('.navbar-collapse').collapse('hide');
  }
});

//Banner Slider

$(document).ready(function () {
  //b_video.play();
  var b_video = $('.banner-video').get(0);
  
  owl2 = $('#banner_slider').owlCarousel({
    items: 1, 
    loop: false,
    rewind: true,
    margin: 0,
    nav: false,
    mouseDrag: true,
    autoplay: true,
    video: true,
    lazyLoad: true,    
    autoplayTimeout: 21000,
    onDragged: resetTimeOut,
    animateOut: 'slideOutUp',
    dots: true,
    dotsData: false,    
    
  });
 
  function resetTimeOut(e) {
    owl2.trigger('stop.owl.autoplay');
    owl2.trigger('play.owl.autoplay');
  }
  owl2.on('click', '.owl-dots, .owl-nav', function(e) {
    resetTimeOut();
  });    

  $('#banner_slider').on('changed.owl.carousel', function(event) {
    $(".owl-item ").each(function(index, value) {
      b_video.pause();
       b_video.currentTime = 0;
 
    });
    $("#banner_slider .owl-item.active").each(function(index, value) {      
      b_video.play();
    });    
  })

  
});

$(window).scroll(function() {
  var header = $(document).scrollTop();
  var headerHeight = $("#myHeader").outerHeight();
  var firstSection =600;
  if (header > headerHeight) {
    $("#myHeader").addClass("in-view");
  } else {
    $("#myHeader").removeClass("in-view");
  }
  if (header > firstSection) {
    $("#myHeader").addClass("fixed");
  } else {
    $("#myHeader").removeClass("fixed");
  }
});
// Equal Height box
if ($(window).width() >= 768) {
$(document).ready(function () {
  $('#seo').each(function () {
    var highestBox = 0;
    $('.equal_height1 .icon_box', this).each(function () {
      if ($(this).height() > highestBox) {
        highestBox = $(this).height();
      }
    });
    $('.equal_height1 .icon_box', this).height(highestBox);
  });
});

$(document).ready(function () {
  $('#seo').each(function () {
    var highestBox = 0;
    $('.equal_height2 .icon_box', this).each(function () {
      if ($(this).height() > highestBox) {
        highestBox = $(this).height();
      }
    });
    $('.equal_height2 .icon_box', this).height(highestBox);
  });
});

$(document).ready(function () {
  $('#seo').each(function () {
    var highestBox = 0;
    $('.equal_height3 .icon_box', this).each(function () {
      if ($(this).height() > highestBox) {
        highestBox = $(this).height();
      }
    });
    $('.equal_height3 .icon_box', this).height(highestBox);
  });
});

$(document).ready(function () {
  $('#seo').each(function () {
    var highestBox = 0;
    $('.equal_height4 .icon_box', this).each(function () {
      if ($(this).height() > highestBox) {
        highestBox = $(this).height();
      }
    });
    $('.equal_height4 .icon_box', this).height(highestBox);
  });
});

};



//testimonial Slider
$(document).ready(function () {
  $("#testimonial-slider").owlCarousel({
    items: 1,
    pagination: true,
    autoPlay: true,
    nav: true,
    navText: ['A', 'B'],
    itemsDesktop: [1000, 1],

  });

  $("#testimonial-slider .owl-next").html('<img src="https://amazing7studio.com/enowate/wp-content/themes/enowate/images/icons/right-arrow.svg" />');
  $("#testimonial-slider .owl-prev").html('<img src="https://amazing7studio.com/enowate/wp-content/themes/enowate/images/icons/left-arrow.svg" />');
});

//Blog Slider

$(document).ready(function () {
  
  $("#blog_slider").owlCarousel({    
    margin: 30,
    nav: true,
    loop: false,
    dots: false,
    autoPlay: true,
    navText: ['<img src="https://amazing7studio.com/enowate/wp-content/themes/enowate/images/icons/arrow-left.svg" />', '<img src="https://amazing7studio.com/enowate/wp-content/themes/enowate/images/icons/arrow-right.svg" />'],
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,

      },
      572: {
        items: 1,

      },
      992: {
        items: 2,

      },
      1000: {
        items: 3,
        
      }
    }

  });
});


//brand Slider

$(document).ready(function () {
  $("#brand-slider").owlCarousel({   
    loop:true,
    margin:10,
    nav:false,
    dots:false,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    responsive: {
      0: {
        items: 1,

      },
      600: {
        items: 3,

      },      
      1000: {
        items: 4,
        
      }
    }
  });
  
});


$(document).ready(function () {
  $("#about-slider").owlCarousel({   
    loop:true,
    margin:30,
    nav:false,
    dots:false,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    responsive: {
      0: {
        items: 2,
        margin:15
      },
      992: {
        items: 3,
        margin:15

      },      
      1300: {
        items: 3,
        
      }
    }
  });
  
});

// equal height box in about us page

$(document).ready(function () {
  if ($(window).width() >= 768) {
    // Select and loop the container element of the elements you want to equalise
    $('.box_grid').each(function () {
      // Cache the highest
      var highestBox = 0;
      // Select and loop the elements you want to equalise
      $('.icon_box', this).each(function () {
        // If this box is higher than the cached highest then store it
        if ($(this).height() > highestBox) {
          highestBox = $(this).height();
        }

      });

      // Set the height of all those children to whichever was highest 
      $('.icon_box', this).height(highestBox);

    });
  }
});

//*******************/
//*******************/

(function ($) {	

  $.fn.searchBox = function(ev) {
  
    var $searchEl = $('.search-elem');
    var $placeHolder = $('.placeholder');
    var $sField = $('#search-field');
  
    if ( ev === "open") {
      $searchEl.addClass('search-open')
    };
  
    if ( ev === 'close') {
      $searchEl.removeClass('search-open'),
      $placeHolder.removeClass('move-up'),
      $sField.val(''); 
    };
  
    var moveText = function() {
      $placeHolder.addClass('move-up');
    }
  
    $sField.focus(moveText);
    $placeHolder.on('click', moveText);
    
    $('.submit').prop('disabled', true);
    $('#search-field').keyup(function() {
          if($(this).val() != '') {
             $('.submit').prop('disabled', false);
          }
      });
  }	
  
  }(jQuery));
  
  $('.search-btn').on('click', function(e) {
  $(this).searchBox('open');
  e.preventDefault();
  });
  
  $('.close').on('click', function() {
  $(this).searchBox('close');
  });

