<?php get_header();



?>

<!--Banner Part-->

<section class="page_banner" style="background-image:url('<?php echo esc_url(get_template_directory_uri()); ?>/images/blog-banner.jpg');">

</section>

<!--********** -->


<section class="single-blog-sec1 py-5 mt-md-4 mt-0 mb-4 mb-md-0">
	<div class="container">
		<div class="row align-items-center justify-content-center">
			<div class="col-md-12 col-xxl-11">
				<div class="single_blog_post">
					<div class="row">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<?php $postid = get_the_ID();

							$category_detail = get_the_category($postid); //$post->ID
							foreach ($category_detail as $cd) {
								//print_r($cd);
								$id = $cd->ID;
							}
							?>

						<div class="col-md-12 col-lg-8">
							<div class="full_post">
								<div class="blog_box blog_style2">
									<div class="blog_title p-0">
										<div class="d-flex align-items-center mb-2">
										<?php $categories = get_the_category();
                      if ( ! empty( $categories ) ) {
                          echo '<span>' . esc_html( $categories[0]->name ) . '</span>';
                      }?>
											<p><?php echo get_the_date(); ?></p>
										</div>
										<h2 class="fw-bold"><?php the_title(); ?></h2>
									
									</div>
									<div class="feacture_img pt-4">
										<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" class="img-fluid">
									</div>
									<div class="pt-4">
										<p><?php echo get_the_content(); ?></p>
									</div>

								</div>
							</div>
						</div>

						<?php endwhile;
										else : ?>
								<p>
									<?php _e('Sorry, no posts matched your criteria.'); ?>
								</p>
							<?php endif; ?>

						 <div class="col-lg-4 col-md-12 mt-4 mt-lg-0">
							<div class="recent_blogs ms-xxl-3 ms-xl-1">
								<h2 class="mb-0">Other Post</h2>
								<div class="other_blog_box">
								<?php $posts = new WP_Query(array('post_type' => 'blog', 'order' => 'ASC', 'posts_per_page' => '5')); ?>

									<?php while ($posts->have_posts()) : $posts->the_post(); ?>

									<div class="blog_box blog_style2">
										<a href="<?php the_permalink(); ?>">
											<div class="img_box">
												<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" class="img-fluid">
											</div>

											<div class="blog_title">
												<div class="d-flex align-items-center mb-1">
												<?php $categories = get_the_category();
                      if ( ! empty( $categories ) ) {
                          echo '<span>' . esc_html( $categories[0]->name ) . '</span>';
                      }?>
													<p><?php echo get_the_date(); ?></p>
												</div>
												<h3><?php echo wp_trim_words(get_the_title(), 8, '...'); ?></h3>
											</div>
										</a>
									</div>
									<?php endwhile; ?>
									<!-- <div class="blog_box blog_style2">
										<a href="javascript:void(0)">
											<div class="img_box">
												<img src="images/blog6.jpg" class="img-fluid">
											</div>

											<div class="blog_title">
												<div class="d-flex align-items-center mb-1">
													<span>SEM</span>
													<p>July 27, 2021</p>
												</div>
												<h3>Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</h3>
											</div>
										</a>
									</div>
									<div class="blog_box blog_style2">
										<a href="javascript:void(0)">
											<div class="img_box">
												<img src="images/blog8.jpg" class="img-fluid">
											</div>

											<div class="blog_title">
												<div class="d-flex align-items-center mb-1">
													<span>SEO</span>
													<p>July 27, 2021</p>
												</div>
												<h3>Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</h3>
											</div>
										</a>
									</div>
									<div class="blog_box blog_style2">
										<a href="javascript:void(0)">
											<div class="img_box">
												<img src="images/blog9.jpg" class="img-fluid">
											</div>

											<div class="blog_title">
												<div class="d-flex align-items-center mb-1">
													<span>SEM</span>
													<p>July 27, 2021</p>
												</div>
												<h3>Doloremque laudantium, totam rem aperiam, eaque ipsa quae.</h3>
											</div>
										</a>
									</div> -->
								</div>
							</div>

						</div>


					</div>


				</div>
			</div>

		</div>
	</div>
</section>

<?php get_footer(); ?>