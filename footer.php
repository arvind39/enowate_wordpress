<footer class="footer_inner">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="footer_bottom">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-3 col-lg-5">
                  <div class="footer_logo">
                    <a href="<?php echo get_site_url(); ?>">
                      <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/footer-logo.png" class="img-fluid">
                    </a>

                  </div>



                </div>
                <div class="col-md-9 col-lg-7 ps-xl-5 ps-md-5 ps-lg-0">
                  <div class="footer_box mt-4 mt-md-0">
                    <div class="row flex-column  flex-md-row">
                      <div class="col mb-4 mb-md-0">
                        <p>Phone no</p>

                        <a href="tel:<?php
                                      if (is_active_sidebar('sidebar-2')) : //check the sidebar if used.
                                        dynamic_sidebar('sidebar-2');  // show the sidebar.
                                      endif;  ?>"> <?php
                                                    if (is_active_sidebar('sidebar-2')) : //check the sidebar if used.
                                                      dynamic_sidebar('sidebar-2');  // show the sidebar.
                                                    endif;
                                                    ?></a>
                      </div>
                      <div class="col mb-4 mb-md-0">
                        <p>Email</p>
                        <a href="mailto:<?php
                                        if (is_active_sidebar('sidebar-3')) : //check the sidebar if used.
                                          dynamic_sidebar('sidebar-3');  // show the sidebar.
                                        endif;
                                        ?>"><?php
                                            if (is_active_sidebar('sidebar-3')) : //check the sidebar if used.
                                              dynamic_sidebar('sidebar-3');  // show the sidebar.
                                            endif;
                                            ?></a>
                      </div>
                      <div class="col mb-4 mb-md-0">
                        <p>Social Links</p>
                        <?php
                        if (is_active_sidebar('sidebar-4')) : //check the sidebar if used.
                          dynamic_sidebar('sidebar-4');  // show the sidebar.
                        endif;
                        ?>
                        <!-- <ul>
                            <li>
                              <a href="https://www.facebook.com/" target="_blank">
                                <img src="<?php //echo esc_url( get_template_directory_uri() ); 
                                          ?>/images/icons/fb.svg" class="img-fluid">
                              </a>
                            </li>
                            <li><a href="https://twitter.com/?lang=en" target="_blank">
                                <img src="<?php //echo esc_url( get_template_directory_uri() ); 
                                          ?>/images/icons/insta.svg" class="img-fluid">
                              </a></li>
                            <li><a href="https://www.linkedin.com/" target="_blank">
                                <img src="<?php //echo esc_url( get_template_directory_uri() ); 
                                          ?>/images/icons/linkedin.svg" class="img-fluid">
                              </a></li>
                          </ul> -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="copyright mt-4 d-flex flex-column flex-md-row align-items-center justify-content-between">
                <?php
                if (is_active_sidebar('sidebar-1')) : //check the sidebar if used.
                  dynamic_sidebar('sidebar-1');  // show the sidebar.
                endif;
                ?>
                <!-- <ul>
                    <li><a href="<?php echo get_site_url(); ?>/term-of-use">Terms of use</a></li>
                    <li><a href="<?php echo get_site_url(); ?>/privacy-policy">Privacy policy</a></li>
                  </ul> -->
                <p>Copyright &copy; 2021 · Powered by <a href="https://www.plaxonic.com/" target="_blank">Plaxonic</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</footer>
<div class="search-box search-elem">
  <button class="close">x</button>
  <div class="inner row">
    <form id="labnol" method="get" action="<?php echo esc_url(home_url('/')); ?>">
      <div class="speech">
        <img class="hold-me" onclick="startDictation()" src="https://image.flaticon.com/icons/svg/26/26312.svg" />
        <input type="text" class="search-query form-control" value="<?php echo get_search_query(); ?>" name="s" id="transcript" />
      </div>
    </form>
  </div>


</div>
</body>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/jquery-3.4.0.min.js"></script>
<!-- <script src="js/jquery-ui.min.js"></script> -->
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/wow.min.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/owl.carousel.min.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/fluidplayer.min.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/jquery.simplePagination.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/custom.js"></script>

<script>
  function startDictation() {

    if (window.hasOwnProperty('webkitSpeechRecognition')) {

      var recognition = new webkitSpeechRecognition();

      recognition.continuous = false;
      recognition.interimResults = false;

      recognition.lang = "en-US";
      recognition.start();

      recognition.onresult = function(e) {
        document.getElementById('transcript').value = e.results[0][0].transcript;
        recognition.stop();
        document.getElementById('labnol').submit();
      };

      recognition.onerror = function(e) {
        recognition.stop();
      }

    }
  }
</script>
<script type='text/javascript' data-cfasync='false'>
  window.purechatApi = {
    l: [],
    t: [],
    on: function() {
      this.l.push(arguments);
    }
  };
  (function() {
    var done = false;
    var script = document.createElement('script');
    script.async = true;
    script.type = 'text/javascript';
    script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';
    document.getElementsByTagName('HEAD').item(0).appendChild(script);
    script.onreadystatechange = script.onload = function(e) {
      if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
        var w = new PCWidget({
          c: '2b2a3158-a004-431a-a434-6b60351c9fc0',
          f: true
        });
        done = true;
      }
    };
  })();
</script>

</html>