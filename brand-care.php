<?php

/**
/* Template Name: Brand Care
 *
 *
 *
 * @package WordPress
 * @subpackage enowate
 * @since enowate 1.0
 */
get_header(); ?>

<?php
//Section 1
$post1 = get_post(225);
$post1_title = $post1->post_title;
$post1_text = $post1->post_content;
$post1_img = get_the_post_thumbnail_url($post1, 'thumbnail');

// Section 5
$post2 = get_post(235);
$post2_title = $post2->post_title;
$post2_text = $post2->post_content;
$post2_img = get_the_post_thumbnail_url($post2, 'thumbnail');

?>

<!--Banner Part-->

<?php
$image_url = wp_get_attachment_url(get_post_thumbnail_id());
if (!empty(get_the_post_thumbnail())) {
?>
  <section class="page_banner" style="background-image:url('<?php echo $image_url; ?>"></section>
<?php } else { ?>
  <section class="page_banner" style="background-image:url('<?php echo esc_url(get_template_directory_uri()); ?>/images/brand-care.jpg');">

  </section>
<?php } ?>

<!--********** -->



<section class="brand-care-sec1 pt-md-5 mb-xxl-4 mb-lg-5 mb-4 mt-md-4 mt-5">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-xxl-9 col-lg-11 col-md-12">
        <div class="content_box pb-4 text-center wow fadeIn">
          <h2 class="pb-2 px-lg-4"><?php echo $post1_title ?></h2>
          <p><?php echo $post1_text ?></p>
        </div>
      </div>

      <div class="col-md-10">
        <div class=" video_box pt-4 pt-md-0 wow zoomIn text-center pb-5 pt-3">
          <button type="button" class="video_btn" data-bs-toggle="modal" data-bs-target="#video_modal">
            <img src="<?php echo $post1_img ?>" class="img-fluid">
          </button>
          <div class="modal fade" id="video_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
              <div class="modal-content">

                <div class="modal-body p-0">
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  <video id="brand-care">
                    <source src="<?php echo esc_url(get_template_directory_uri()); ?>/video/enowate.mp4" type="video/mp4" width="100%" />
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>




</section>
<section class="brand-care-sec2 bg_dark text_white">
  <div class="container">
    <div class="col-md-12">
      <div class="default_title text-center pt-5 pb-md-4 mb-4">
        <h2>Importance of brand promotion</h2>
      </div>
    </div>
    <div class="row align-items-center">
      <?php $posts = new WP_Query(array('post_type' => 'brand_care', 'category_name' => 'Brand Promotion',  'order' => 'ASC', 'posts_per_page' => '4')); ?>

      <?php while ($posts->have_posts()) : $posts->the_post(); ?>
        <div class="col-md-6">
          <div class="icon_box box_style2 mb-4">
            <div class="icon black_white mb-md-4 mb-3">
              <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="img-fluid">

            </div>
            <h4><?php echo get_the_title(); ?></h4>
            <small><?php echo get_the_content(); ?></small> </p>

          </div>
        </div>
      <?php endwhile; ?>
      <!-- <div class="col-md-6">
          <div class="icon_box box_style2 mb-4">
            <div class="icon mb-md-4 mb-3">
              <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icons/iw2.svg" class="img-fluid">
              <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icons/i2.svg" class="img-fluid i_move">
            </div>
            <h4>Personal Selling</h4>
            <small>Our strong two-way personal selling communication technique helps to sell the product and allows
              us to build relationships to prevent customer attrition. </small>

          </div>
        </div>
        <div class="col-md-6">
          <div class="icon_box box_style2 mb-4">
            <div class="icon mb-md-4 mb-3">
              <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icons/iw3.svg" class="img-fluid">
              <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icons/i3.svg" class="img-fluid i_move">
            </div>
            <h4>Sales Promotion</h4>
            <small>Our effective sales promotion techniques help companies to drive sales through ROI maximizing
              campaigns. </small>

          </div>
        </div>
        <div class="col-md-6">
          <div class="icon_box box_style2 mb-4">
            <div class="icon mb-md-4 mb-3">
              <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icons/iw4.svg" class="img-fluid">
              <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icons/i4.svg" class="img-fluid i_move">
            </div>
            <h4>360° Campaign</h4>
            <small>We implement 360-degree integrated marketing campaigns to promote brands successfully. </small>

          </div>
        </div> -->
    </div>
  </div>
</section>
<section class="brand-care-sec3">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-12">
        <div class="content_box pe-xxl-5 wow fadeIn">
          <h2 class="pb-2"><?php echo $post2_title ?></h2>
          <p><?php echo $post2_text ?></p>
          <!-- <ul class="list_style2 ">
            <li>
              <h4>Sales Promotion</h4>
              <p>We do promote our clients' brands to a broad audience by using the best digital marketing tools on
                various digital platforms. </p>
            </li>
            <li>
              <h4>Cost-effective and More Revenue Yielding</h4>
              <p>We use different marketing channels for marketing the clients' products to a specific target audience.
              </p>
            </li>
            <li>
              <h4>Implement Measurable Branding Strategy </h4>
              <p>We use effective online marketing strategies that allow us to track clients' marketed campaigns. Our
                implemented strategies help clients to grow their businesses in the long run.</p>
            </li>
            <li>
              <h4>High Conversation Rates</h4>
              <p>We increase the visibility of clients' business by promoting it on different digital marketing channels
                to increase online traffic and generate more revenue.</p>
            </li>

          </ul> -->
        </div>
      </div>

    </div>
  </div>
</section>
<section class="brand-care-sec5 bg_yellow py-4">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="brand-slider" class="owl-carousel">
          <?php $posts = new WP_Query(array('post_type' => 'brand_care', 'category_name' => 'brand logo',  'order' => 'ASC', 'posts_per_page' => '-1')); ?>

          <?php while ($posts->have_posts()) : $posts->the_post(); ?>
          <div class="brand_icon">
            <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="img-fluid">
          </div>
          <?php endwhile; ?>
          <!-- <div class="brand_icon">
            <img src="<?php //echo esc_url(get_template_directory_uri()); ?>/images/brand2.png" class="img-fluid">
          </div>
          <div class="brand_icon">
            <img src="<?php //echo esc_url(get_template_directory_uri()); ?>/images/brand3.png" class="img-fluid">
          </div>
          <div class="brand_icon">
            <img src="<?php //echo esc_url(get_template_directory_uri()); ?>/images/braand4.png" class="img-fluid">
          </div> -->
        </div>

      </div>
    </div>

  </div>
</section>




<?php get_footer(); ?>
<script>
  $('#video_modal').on('shown.bs.modal', function() {
    var myFP = fluidPlayer(
      'brand-care', {
        "layoutControls": {
          "controlBar": {
            "autoHideTimeout": 3,
            "animated": true,
            "autoHide": true
          },
          "htmlOnPauseBlock": {
            "html": null,
            "height": null,
            "width": null
          },
          "autoPlay": true,
          "mute": false,
          "allowTheatre": false,
          "playPauseAnimation": true,
          "playbackRateEnabled": false,
          "allowDownload": false,
          "playButtonShowing": false,
          "fillToContainer": true,
          "fillToContainer": false,
          "primaryColor": "#FFE715",
          "fullscreen": true,
          
        },
        "vastOptions": {
          "adList": [],
          "adCTAText": false,
          "adCTATextPosition": ""
        }
      });
  })
  $('#video_modal').on('hidden.bs.modal', function() {
    $('#brand-care')[0].pause();
    $('#brand-care').get(0).currentTime = 0;
  })
</script>