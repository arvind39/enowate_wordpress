<?php

/**
/* Template Name: Home
 *
 * Displays Only Home template
 *
 * @package WordPress
 * @subpackage enowate
 * @since enowate 1.0
 */
get_header(); ?>

<?php

//Home Section 1
$home_post1 = get_post(50);
$home_post1_title = $home_post1->post_title;
$home_post1_text = $home_post1->post_content;
$home_post1_img = get_the_post_thumbnail_url($home_post1, 'thumbnail');

//Home Section 2
$home_post2 = get_post(110);
$home_post2_title = $home_post2->post_title;
$home_post2_text = $home_post2->post_content;
$home_post2_img = get_the_post_thumbnail_url($home_post2, 'thumbnail');

//Home Section 3
$home_post3 = get_post(111);
$home_post3_title = $home_post3->post_title;
$home_post3_text = $home_post3->post_content;
$home_post3_img = get_the_post_thumbnail_url($home_post3, 'thumbnail');

?>

<section class="home_sec1">
  <div class="main_slider">
    <div class="owl-carousel owl-theme" id="banner_slider">


      <?php $posts = new WP_Query(array('post_type' => 'Slider', 'category_name' => 'Banner Video',  'order' => 'ASC')); ?>

      <?php while ($posts->have_posts()) : $posts->the_post(); ?>
        <div class="item">
          <video id="banner_video" class="w-100 banner-video" poster="<?php echo esc_url(get_template_directory_uri());
                                                                      ?>/images/banner-1.jpg" muted autoplay>
            <source src="<?php echo get_the_content(); ?>" type="video/mp4" />
          </video>
          <div class="cover">
            <div class="container">

              <div class="row">
                <div class="col-md-6">
                  <div class="">
                    <div class="header-content">
                      <h2><?php echo (get_the_excerpt()); ?></h2>
                    </div>
                  </div>
                  <a href="<?php echo get_site_url(); ?>/brand-care" class="cta_btn cta_big">
                    KNOW MORE <svg class="
                                ms-3" width="10" height="10" viewBox="0 0 10 10" fill="#000" xmlns="http://www.w3.org/2000/svg">
                      <path d="M3.45999 10V6.3H0V3.68H3.45999V0H6.25999V3.68H9.72V6.3H6.25999V10H3.45999Z" />
                    </svg>
                  </a>
                </div>
                <div class="col-md-4">
                </div>
              </div>
            </div>
          </div>
        </div>

      <?php endwhile; ?>

      <?php $posts = new WP_Query(array('post_type' => 'Slider', 'category_name' => 'Home Slider', 'order' => 'ASC')); ?>

      <?php while ($posts->have_posts()) : $posts->the_post(); ?>
        <div class="item">
          <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" alt="images not found">
          <div class="cover">
            <div class="container">
              <div class="row">
                <div class="col-md-6 ">
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="header-content">
                      <h2><?php echo get_the_content(); ?></h2>
                      <!-- <h2>Thrive your business with up-to-date innovations</h2> -->
                    </div>
                  </div>
                  <a href="<?php echo get_site_url(); ?>/<?php echo (get_the_excerpt()); ?>" class="cta_btn cta_big">
                    KNOW MORE <svg class="
                    ms-3" width="10" height="10" viewBox="0 0 10 10" fill="#000" xmlns="http://www.w3.org/2000/svg">
                      <path d="M3.45999 10V6.3H0V3.68H3.45999V0H6.25999V3.68H9.72V6.3H6.25999V10H3.45999Z" />
                    </svg>
                  </a>
                </div>
                <div class="col-md-4">
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endwhile; ?>
    </div>
</section>
<section class="home_sec2 py-4 py-lg-5 my-5">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-6">
        <div class="content_box pe-lg-4  wow fadeInUp">
          <h2 class="pb-2"><?php echo $home_post1_title ?></h2>

          <p><?php echo $home_post1_text ?></p>
          <a href="<?php echo get_site_url(); ?>/about-us" class="cta_btn cta_big cta_black mt-4">
            KNOW MORE <svg class="
              ms-3" width="10" height="10" viewBox="0 0 10 10" fill="#000" xmlns="http://www.w3.org/2000/svg">
              <path d="M3.45999 10V6.3H0V3.68H3.45999V0H6.25999V3.68H9.72V6.3H6.25999V10H3.45999Z" />
            </svg>
          </a>          
        </div>
      </div>
      <div class="col-md-6">
        <div class="img_box p-md-4 p-lg-0 pt-5 pt-md-0 wow zoomIn text-end">
          <img src="<?php echo $home_post1_img; ?>" class="img-fluid">
        </div>
      </div>
    </div>
  </div>
</section>
<section class="home_sec3">
  <div class="container">
    <div class="row align-items-center flex-column-reverse flex-md-row">

      <div class="col-md-6">
        <div class="img_box p-4 p-lg-0 pt-5 pt-md-0 wow zoomIn">
          <img src="<?php echo $home_post2_img ?>" class="img-fluid">
        </div>
      </div>
      <div class="col-md-6">
        <div class="content_box  wow fadeIn">
          <h2 class="pb-2"><?php echo $home_post2_title ?></h2>
          <p><?php echo $home_post2_text ?></p>
          <a href="<?php echo get_site_url(); ?>/other-services#otherservices" class="cta_btn cta_big cta_black mt-4">
            KNOW MORE <svg class="
              ms-3" width="10" height="10" viewBox="0 0 10 10" fill="#000" xmlns="http://www.w3.org/2000/svg">
              <path d="M3.45999 10V6.3H0V3.68H3.45999V0H6.25999V3.68H9.72V6.3H6.25999V10H3.45999Z" />
            </svg>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="home_sec4 py-5  my-lg-5 my-4">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-6">
        <div class="content_box pe-lg-4 wow fadeIn">
          <h2 class="pb-2"><?php echo $home_post3_title ?></h2>
          <p><?php echo $home_post3_text ?></p>
          <a href="<?php echo get_site_url(); ?>/other-services#contentwriting" class="cta_btn cta_big cta_black mt-4">
            KNOW MORE <svg class="
              ms-3" width="10" height="10" viewBox="0 0 10 10" fill="#000" xmlns="http://www.w3.org/2000/svg">
              <path d="M3.45999 10V6.3H0V3.68H3.45999V0H6.25999V3.68H9.72V6.3H6.25999V10H3.45999Z" />
            </svg>
          </a>
        </div>
      </div>
      <div class="col-md-6">
        <div class="img_box p-4 p-lg-0 pt-5 pt-md-0 wow zoomIn text-end">
          <img src="<?php echo $home_post3_img ?>" class="img-fluid">
        </div>
      </div>
    </div>
  </div>
</section>
<section class="home_sec5">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-lg-5 ps-5 ps-xl-0 pb-5 pb-lg-0">
        <div class="default_title position-relative">
          <div class="quote">

            <svg width="21" height="17" viewBox="0 0 21 17" fill="#101920" xmlns="http://www.w3.org/2000/svg">
              <path d="M0 16.632V9.296C0 6.57067 0.802667 4.40533 2.408 2.8C3.976 1.19467 6.01067 0.261334 8.512 0V3.248C7.35467 3.584 6.40267 4.21867 5.656 5.152C4.90933 6.048 4.536 7.09333 4.536 8.288H8.344V16.632H0ZM11.76 16.632V9.296C11.76 6.57067 12.5627 4.40533 14.168 2.8C15.736 1.19467 17.7707 0.261334 20.272 0V3.248C19.1147 3.584 18.1627 4.21867 17.416 5.152C16.6693 6.048 16.296 7.09333 16.296 8.288H20.104V16.632H11.76Z" />
            </svg>
          </div>
          <h2>What our customers are saying</h2>
        </div>
      </div>
      <div class="col-md-12 col-lg-7">
        <div id="testimonial-slider" class="owl-carousel">
          <?php $posts = new WP_Query(array('post_type' => 'testimonial', 'order' => 'ASC')); ?>

          <?php while ($posts->have_posts()) : $posts->the_post(); ?>
            <div class="testimonial">
              <div class="description">
                <h3><?php echo get_the_title(); ?></h3>
                <p><?php echo get_the_content(); ?></p>
              </div>
              <div class="testimonial-review mt-4 d-flex align-items-center">
                <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" class="img-fluid">
                <h4 class="ms-3"><?php echo (get_the_excerpt()); ?></h4>
              </div>
            </div>
          <?php endwhile; ?>
        
        </div>
      </div>
    </div>
  </div>
</section>
<section class="home_sec6">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="default_title text-center mb-5 pb-4">
          <h2>Latest from Our Blog</h2>
          <p>Read on to discover what’s trending around the world of online marketing.</p>
        </div>
      </div>
      <div class="col-md-12">
        <div id="blog_slider" class="owl-carousel">
        <?php $posts = new WP_Query(array('post_type' => 'blog', 'order' => 'ASC')); ?>

        <?php while ($posts->have_posts()) : $posts->the_post(); ?>
       
          <div class="blog_box">
            <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" class="img-fluid">
            <div class="blog_title">
              <a href="<?php the_permalink(); ?>">
                <div class="d-flex align-items-center mb-2">

                <?php $categories = get_the_category();
                if ( ! empty( $categories ) ) {
                    echo '<span>' . esc_html( $categories[0]->name ) . '</span>';
                }?>
                 
                  
                  <p><?php echo get_the_date(); ?></p>
                </div>
                <h3><?php echo get_the_title(); ?></h3>

                
               
              </a>
            </div>
          </div>
        <?php endwhile; ?>
          <!-- <div class="blog_box">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/blog2.jpg" class="img-fluid">
            <div class="blog_title">
              <a href="single-blog.html">
                <div class="d-flex align-items-center mb-2">
                  <span>PPC</span>
                  <p>July 28, 2021</p>
                </div>
                <h3>Report: 51% of Web Site Hacks Related to SEO Spam</h3>
              </a>
            </div>
          </div>
          <div class="blog_box">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/blog3.jpg" class="img-fluid">
            <div class="blog_title">
              <a href="single-blog.html">
                <div class="d-flex align-items-center mb-2">
                  <span>SEM</span>
                  <p>July 27, 2021</p>
                </div>
                <h3>The Future of Search Engine Marketing is Full Funnel</h3>
              </a>
            </div>
          </div>
          <div class="blog_box">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/blog1.jpg" class="img-fluid">
            <div class="blog_title">
              <a href="single-blog.html">
                <div class="d-flex align-items-center mb-2">
                  <span>SEO</span>
                  <p>July 28, 2021</p>
                </div>
                <h3>Maximize Clicks in Bing Ads Now Available Globally</h3>
              </a>
            </div>
          </div> -->
        </div>
        <div class="text-center mt-5">
          <a href="<?php echo get_site_url(); ?>/blogs" class="cta_btn cta_big">
            VIEW ALL <svg class="
          ms-3" width="10" height="10" viewBox="0 0 10 10" fill="#000" xmlns="http://www.w3.org/2000/svg">
              <path d="M3.45999 10V6.3H0V3.68H3.45999V0H6.25999V3.68H9.72V6.3H6.25999V10H3.45999Z"></path>
            </svg>
          </a>
        </div>

      </div>
    </div>
  </div>
</section>
<section class="home_sec7">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="home_contact">
          <div class="row">
            <div class="col-md-5 pe-4 pe-lg-5 mb-4   mb-md-0">
              <div class="default_title wow fadeInUp">
                <span>Connect</span>
                <h2 class="my-4">Let's Talk</h2>
                <h4>For further inquiries or assistance, please fill out the form below and contact us.</h4>
              </div>
            </div>
            <div class="col-md-7">
              <div class="contact_form">
             <!-- <form action="javascript:void(0)">

                  <div class="row">
                    <div class="col-md-6 col-12">
                      <fieldset class="mb-5">

                        <input type="text" name="name" class="form-control" placeholder="Full Name*" required>
                      </fieldset>
                    </div>

                    <div class="col-md-6 col-12">
                      <fieldset class="mb-5">

                        <input type="text" name="phone-number" pattern="\d{10}" title="Only 10 digits mobile number" class="form-control" placeholder="Phone No*" required>
                      </fieldset>
                    </div>
                    <div class="col-md-12 col-12">
                      <fieldset class="mb-5">
                        <input type="email" class="form-control" placeholder="Email Address*" required>
                      </fieldset>
                    </div>

                    <div class="col-md-8 col-xl-9 col-12">
                      <fieldset class="mb-4">

                        <textarea cols="10" rows="3" class="form-control shadow-none" minlength="25" placeholder="Your Message*" required></textarea>
                      </fieldset>
                    </div>

                    <div class="col-md-3 col-xl-3 col-12 d-flex align-items-lg-center align-items-end">
                      <div class="send_btn">
                        <input type="submit" class="input_field_send" value="Send">
                        <span>SEND</span>
                        <svg class="ms-3" width="10" height="10" viewBox="0 0 10 10" fill="#000" xmlns="http://www.w3.org/2000/svg">
                          <path d="M3.45999 10V6.3H0V3.68H3.45999V0H6.25999V3.68H9.72V6.3H6.25999V10H3.45999Z"></path>
                        </svg>
                      </div>
                    </div>
                  </div>
                </form> -->
                <?php echo do_shortcode('[contact-form-7 id="304" title="Contact form"]');?>
                
               

              </div>
            </div>
          </div>
        </div>
        <hr class="my-5">
      </div>

    </div>

  </div>
</section>
<?php get_footer(); ?>