<?php
/**
/* Template Name: Influencer Marketing
 *
 *
 *
 * @package WordPress
 * @subpackage enowate
 * @since enowate 1.0
 */
 get_header(); ?>

<?php

//Section 1
$post1 = get_post(205);
$post1_title = $post1->post_title;
$post1_text = $post1->post_content;
$post1_img = get_the_post_thumbnail_url($post1, 'thumbnail');

// Section 2
$post2 = get_post(206);
$post2_title = $post2->post_title;
$post2_text = $post2->post_content;
$post2_img = get_the_post_thumbnail_url($post2, 'thumbnail');

?>

<!--Banner Part-->

<?php
  $image_url= wp_get_attachment_url( get_post_thumbnail_id() );
  if( !empty(get_the_post_thumbnail()) ) {
?>
<section class="page_banner" style="background-image:url('<?php echo $image_url;?>"></section>
<?php } else { ?>
  <section class="page_banner" style="background-image:url('<?php echo esc_url( get_template_directory_uri() ); ?>/images/influencer-arketing-banner.jpg');"></section>
<?php } ?>

<!--********** -->

  <section class="influencer-arketing-sec1 py-5 mb-md-4 mt-md-0 mt-4">
    <div class="container">
      <div class="col-md-12">
        <div class="search_box pb-md-5 pb-3">
          <form class="search" action="javascript:void(0)">
            <fieldset class="d-flex align-items-center justify-content-center">
              <input type="text" class="w-100" placeholder="Search for influencers" name="search">
              <button type="submit"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icons/search.svg"> </button>
            </fieldset>

          </form>
        </div>
      </div>
      <div class="row align-items-center flex-column-reverse flex-md-row mt-md-0 mt-4">

        <div class="col-md-6">
          <div class="img_box p-4 p-lg-0 pt-5 pt-md-0 wow zoomIn">
            <img src="<?php echo $post1_img ?>" class="img-fluid">
          </div>
        </div>
        <div class="col-md-6">
          <div class="content_box  wow fadeIn">
            <h2 class="pb-2"><?php echo $post1_title?></h2>
            <p><?php echo $post1_text?></p>

            <a href="<?php echo get_site_url(); ?>/contact#contactform" class="cta_btn cta_big cta_black mt-md-5 mt-3">
              START NOW <svg class="
                ms-3" width="10" height="10" viewBox="0 0 10 10" fill="#000" xmlns="http://www.w3.org/2000/svg">
                <path d="M3.45999 10V6.3H0V3.68H3.45999V0H6.25999V3.68H9.72V6.3H6.25999V10H3.45999Z" />
              </svg>
            </a>

          </div>
        </div>
      </div>
    </div>

  </section>
  <section class="influencer-arketing-sec2">
    <div class="container">
      <div class="row align-items-center">

        <div class="col-md-6">
          <div class="content_box pe-xxl-5 wow fadeIn">
            <h2 class="pb-2"><?php echo $post2_title?></h2>
            <p><?php echo $post2_text?></p>
            <!-- <ul class="default_list">
              <li>
                The AI technology helps analyze the content and generate search engines.
              </li>
              <li>Helps validate analytics mechanism for better audience interaction.</li>
              <li>Up-to-date tech enables you to create a list of relevant suggestions.</li>
            </ul> -->
          </div>
        </div>
        <div class="col-md-6">
          <div class="img_box p-4 p-lg-0 pt-5 pt-md-0 wow zoomIn text-end">
            <img src="<?php echo $post2_img ?>" class="img-fluid">
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="influencer-arketing-sec3 bg_yellow py-5 mb-md-4 ">
    <div class="container">
      <div class="col-md-12">
        <div class="default_title text-center py-4">
          <h2>Ready to take your influencer marketing campaign to the next level using our services?</h2>
          <a href="<?php echo get_site_url(); ?>/contact#contactform" class="cta_btn cta_big cta_black mt-4">
            HIRE US TODAY <svg class="
              ms-3" width="10" height="10" viewBox="0 0 10 10" fill="#000" xmlns="http://www.w3.org/2000/svg">
              <path d="M3.45999 10V6.3H0V3.68H3.45999V0H6.25999V3.68H9.72V6.3H6.25999V10H3.45999Z" />
            </svg>
          </a>
        </div>

      </div>

    </div>
  </section>
  <section class="influencer-arketing-sec4 py-5 mt-md-5 mt-4 mb-md-4">
    <div class="container">
      <div class="col-md-12">
        <div class="default_title text-center mb-5">
          <h2>How do we make a difference? </h2>
        </div>
      </div>
      <div class="row">


      <?php $posts = new WP_Query(array('post_type' => 'influencer_marketing', 'category_name' => 'Influencer Marketing Feature',  'order' => 'ASC')); ?>

<?php while ($posts->have_posts()) : $posts->the_post(); ?>

        <div class="col-lg-4 col-md-6">
          <div class="icon_box">

            <div class="icon mb-3">
           
            <img src=" <?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" class="img-fluid">
              <!-- <img src="<?php //echo esc_url( get_template_directory_uri() ); ?>/images/icons/s1.svg" class="img-fluid"> -->

            </div>
            <h4><?php echo get_the_title(); ?></h4>
            <small><?php echo get_the_content(); ?></small>
            <!-- <h4>We build a custom strategy.</h4>
            <small>We invest our time learning your business and presenting a set of custom strategies that align
                precisely with your business goal. We utilize trusted analytical tools for a data-driven approach to
                find and approach influencers. </small> </p> -->

          </div>
        </div>

        <?php endwhile; ?>

        <!-- <div class="col-lg-4 col-md-6">
          <div class="icon_box">

            <div class="icon mb-3">
              <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icons/s2.svg" class="img-fluid">
            </div>
            <h4>Find & Select Appropriate Influencer</h4>
            <small>We believe in providing unparalleled results by choosing an influencer that fits appropriately for
                your business goal. Advanced AI technology helps us list all suitable influencers in one place and
                depending on the budget, reach, and influencer model, we select an appropriate influencer. </small>

          </div>
        </div>
        <div class="col-lg-4 col-md-6">
          <div class="icon_box">
            <div class="icon mb-3">
              <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icons/s3.svg" class="img-fluid">
            </div>
            <h4>Run an Outreach Campaign</h4>
            <small>We Identify the influencers based on your target market, contact them and encourage them to
                promote your brand. We help the influencers analyze your purpose and assist with content creation.
              </small>

          </div>
        </div>
        <div class="col-lg-4 col-md-6">
          <div class="icon_box">
            <div class="icon mb-3">
              <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icons/s4.svg" class="img-fluid">
            </div>
            <h4>Set Prices & Negotiate Contracts</h4>
            <small>We are proficient at handling your budget! We fix a price, search for an influencer and negotiate
                with them until they agree to our prices. We don’t let your budget suffer even for a bit. </small>

          </div>
        </div>
        <div class="col-lg-4 col-md-6">
          <div class="icon_box">
            <div class="icon mb-3">
              <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icons/s5.svg" class="img-fluid">
            </div>
            <h4>Manage the campaign end-to-end </h4>
            <small>We handle everything from analyzing your business, preparing strategies, searching for the perfect
                fit, negotiating prices, and creating content. We analyze the process once implemented and provide
                incredible ROI. </small>

          </div>
        </div>
        <div class="col-lg-4 col-md-6">
          <div class="icon_box">
            <div class="icon mb-3">
              <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icons/s6.svg" class="img-fluid">
            </div>
            <h4>Analyze and improve performance</h4>
            <small>From planning to execution, we keep analyzing the strategy and look out for improvisations. We
                ensure to provide successful influencer marketing campaigns that drive expected ROI.</small>

          </div>
        </div> -->
      </div>
    </div>
  </section>
  <?php get_footer(); ?>