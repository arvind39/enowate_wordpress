<?php

/**
/* Template Name: Other Services
 *
 *
 *
 * @package WordPress
 * @subpackage enowate
 * @since enowate 1.0
 */
get_header(); ?>

<?php

//Section 1
$post1 = get_post(244);
$post1_title = $post1->post_title;
$post1_text = $post1->post_content;
$post1_img = get_the_post_thumbnail_url($post1, 'thumbnail');


$post2 = get_post(245);
$post2_title = $post2->post_title;
$post2_text = $post2->post_content;
$post2_img = get_the_post_thumbnail_url($post2, 'thumbnail');

?>

<!--Banner Part-->

<?php
$image_url = wp_get_attachment_url(get_post_thumbnail_id());
if (!empty(get_the_post_thumbnail())) {
?>
  <section class="page_banner" style="background-image:url('<?php echo $image_url; ?>"></section>
<?php } else { ?>
  <section class="page_banner" style="background-image:url('<?php echo esc_url(get_template_directory_uri()); ?>/images/other-services-banner.jpg');">

  </section>
<?php } ?>

<!--********** -->




<section class="other-services-sec1 pt-md-5 mb-xxl-5 mb-lg-5 mb-4 mt-md-4 mt-5">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-xxl-9 col-lg-11 col-md-12">
        <div class="content_box text-center wow fadeIn">
          <h2 class="pb-2 px-lg-4"><?php echo $post1_title ?></h2>
          <p><?php echo $post1_text ?></p>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="otherservices" class="other-services-sec2 bg_dark text_white py-5 mb-4">
  <div class="container">
    <div class="col-md-12">
      <div class="default_title text-center pt-4 pb-md-4 mb-4">
        <h2>Marketing</h2>
        <p>Marketing services that build your brand identity with faster ROI. </p>
      </div>
    </div>
    <div class="row">

      <div class="col-xl-3 col-lg-4 col-md-6">
        <a href="<?php echo get_site_url(); ?>/search-engine-optimization" class="link_box">
          <div class="icon_box box_style2 mb-4">
            <div class="icon black_white mb-lg-4 mb-3">
              <img src="<?php echo $post2_img ?>" class="img-fluid ">
            </div>
            <h4><?php echo $post2_title ?></h4>
            <small><?php echo $post2_text ?></small>
          </div>
        </a>

      </div>

      <?php $posts = new WP_Query(array('post_type' => 'other_services', 'category_name' => 'Marketing',  'order' => 'ASC', 'posts_per_page' => '-1')); ?>


      <?php while ($posts->have_posts()) : $posts->the_post(); ?>


        <div class="col-xl-3 col-lg-4 col-md-6">
          <div class="icon_box box_style2 mb-4">
            <div class="icon black_white mb-lg-4 mb-3">
              <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="img-fluid ">
            </div>
            <h4><?php echo get_the_title(); ?></h4>
            <small><?php echo get_the_content(); ?></small>
          </div>
        </div>



      <?php endwhile; ?>

      <!-- <div class="col-xl-3 col-lg-4 col-md-6">
        <div class="icon_box box_style2 mb-4">
          <div class="icon black_white mb-lg-4 mb-3">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icons/icon2.svg" class="img-fluid ">          
          </div>
          <h4>Social Media Marketing</h4>
          <small>We build social profiles that highlight your strength, maximize brand awareness, and turn your
            business vision into a cohesive social persona. </small>
        </div>
      </div>
       -->
    </div>
  </div>
</section>
<section id="contentwriting" class="other-services-sec3 pt-5 my-xl-5 my-4 ">
  <div class="container">
    <div class="col-md-12">
      <div class="default_title text-center mb-lg-5 mb-4">
        <h2>Content Writing</h2>
        <p>User-friendly, SEO optimized, and plagiarism-free content!</p>
      </div>
    </div>
    <div class="row align-items-center">
      <?php $posts = new WP_Query(array('post_type' => 'other_services', 'category_name' => 'Content Writing',  'order' => 'ASC', 'posts_per_page' => '3')); ?>

      <?php while ($posts->have_posts()) : $posts->the_post(); ?>
        <div class="col-md-4">
          <div class="icon_box d-flex align-items-center justify-content-center flex-column text-center px-xl-4 px-4 px-md-0 mb-5">
            <div class="icon mb-md-4 mb-3">
              <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="img-fluid">
            </div>
            <h4><?php echo get_the_title(); ?></h4>
            <small><?php echo get_the_content(); ?></small>

          </div>
        </div>
      <?php endwhile; ?>
      <!-- <div class="col-md-4">
        <div class="icon_box d-flex align-items-center justify-content-center flex-column text-center px-xl-4 px-4 px-md-0 mb-5">

          <div class="icon mb-md-4 mb-3">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icons/s10.svg" class="img-fluid">
          </div>
          <h4>Subtitles</h4>
          <small>We offer subtitling services for all languages that effectively deliver video messages to a large
            group audience. </small>
        </div>
      </div> -->

    </div>
  </div>
</section>
<?php get_footer(); ?>