<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="user-scalable=no, width=device-width" />
  <title>Enowate</title>
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/favicon.png" />
  <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/animate.min.css" rel="stylesheet">
  <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/video-js.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/main.css" rel="stylesheet" type="text/css" />

</head>

<body>
  <header class="w-100">

    <div class="main_menu" id="myHeader">
      <div class="container-fluid">
        <div class="row">

          <div class="col-md-12">
            <div class="main_header">
              <nav class="navbar navbar-expand-lg p-0">
                <div class="d-flex w-100 d-flex align-items-center justify-content-between">
                  <a class="navbar-brand" href="<?php echo get_site_url(); ?>">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.svg" class="img-fluid">
                  </a>
                  <div class="d-flex align-items-center flex-row-reverse flex-lg-row">
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main_menu"
                      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <div class="bar1"></div>
                      <div class="bar2"></div>
                      <div class="bar3"></div>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end p-4 p-md-0" id="main_menu">
                      <a class="navbar-brand d-lg-none d-block d-flex mb-4 p-2" href="<?php echo get_site_url(); ?>">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.svg" class="img-fluid">
                      </a>

                      <?php   wp_nav_menu( array(
		                                    'theme_location'    => 'primary',
		                                    'depth'             => 2,
		                                    'container'         => 'ul',
		                                    'container_class'   => '',
		                                    'container_id'      => '',
		                                    'menu_class'        => 'navbar-nav mb-2 mb-lg-0',
		                                    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
		                                    'walker'            => new WP_Bootstrap_Navwalker())
		                                );
		                                ?>

                 <!-- <ul class="navbar-nav mb-2 mb-lg-0">
                        <li class="nav-item">
                          <a class="nav-link" href="product-placement.html">Product Placement</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="influencer-marketing.html">Influencer Marketing</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="movie-subtitles.html">Movie Subtitles</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="brand-care.html">Brand Care</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="other-services.html">Other Services</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="blog.html">Blog</a>
                        </li>

                      </ul> -->

                    </div>
                    <a href="<?php echo get_site_url(); ?>/contact" class="cta_btn ms-sm-4 ms-1">
                      Let's Talk <svg class="
                    ms-3" width="10" height="10" viewBox="0 0 10 10" fill="#000" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3.45999 10V6.3H0V3.68H3.45999V0H6.25999V3.68H9.72V6.3H6.25999V10H3.45999Z" />
                      </svg>
                    </a>
                    <a href="#" class="search-btn">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M21.172 24l-7.387-7.387c-1.388.874-3.024 1.387-4.785 1.387-4.971 0-9-4.029-9-9s4.029-9 9-9 9 4.029 9 9c0 1.761-.514 3.398-1.387 4.785l7.387 7.387-2.828 2.828zm-12.172-8c3.859 0 7-3.14 7-7s-3.141-7-7-7-7 3.14-7 7 3.141 7 7 7z"/>
                      </svg>
                    </a>
                  </div>

                </div>
              </nav>
            </div>

          </div>
        </div>
      </div>
    </div>
  </header>